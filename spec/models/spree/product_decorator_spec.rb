require "rails_helper"

RSpec.describe Spree::ProductDecorator, type: :model do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:related_product) { create(:product, taxons: [taxon]) }
  let(:no_related_product) { create(:product) }

  it "related_productは関連商品に含まれていること" do
    expect(product.related_products).to include related_product
  end

  it "no_related_productは関連商品に含まれないこと" do
    expect(product.related_products).not_to include no_related_product
  end

  it "product自身は関連商品に含まれないこと" do
    expect(product.related_products).not_to include product
  end
end
