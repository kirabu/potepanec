RSpec.describe 'Potepan::Categories', type: :system do
  let!(:taxonomy) { create(:taxonomy, name: "Categories") }
  let!(:bag) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:mag) { create(:taxon, name: "Mags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:bag_product) { create(:product, name: "RAILS BAG", price: "15.99", taxons: [bag]) }
  let!(:mag_product) { create(:product, name: "RAILS MAG", price: "16.99", taxons: [mag]) }

  before do
    visit potepan_category_path(bag.id)
  end

  it "Bagカテゴリーページと商品カテゴリーの要素が正しく表示されること" do
    expect(page).to have_title("#{bag.name} - BIGBAG Store")
    within ".productBox" do
      expect(page).to have_content bag_product.name
      expect(page).to have_content bag_product.display_price
      expect(page).not_to have_content mag_product.name
      expect(page).not_to have_content mag_product.display_price
    end
    click_on "Categories"
    within ".side-nav" do
      expect(page).to have_content bag.name
      expect(page).to have_content bag.all_products.count
      expect(page).to have_content mag.name
      expect(page).to have_content mag.all_products.count
    end
  end

  it "Magカテゴリーをクリックするとそのカテゴリーに属する商品のみ表示されること" do
    click_on "Categories"
    click_on "Mag"
    expect(current_path).to eq potepan_category_path(mag.id)
    expect(page).to have_title("#{mag.name} - BIGBAG Store")
    within ".productBox" do
      expect(page).to have_content mag_product.name
      expect(page).to have_content mag_product.display_price
      expect(page).not_to have_content bag_product.name
      expect(page).not_to have_content bag_product.display_price
    end
  end

  it "商品名をクリックするとその商品のリンク先に正しく遷移すること" do
    click_on "RAILS BAG"
    expect(current_path).to eq potepan_product_path(bag_product.id)
  end
end
