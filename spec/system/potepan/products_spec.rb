require 'rails_helper'

RSpec.describe 'Potepan::Products', type: :system do
  let!(:product) { create(:product) }
  let!(:taxonomy) { create(:taxonomy, name: "Categories") }
  let!(:bag) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:bag_product) { create(:product, name: "RAILS BAG", price: "15.99", taxons: [bag]) }
  let!(:related_products) { create_list(:product, 5, taxons: [bag]) }
  let!(:bag_product5) { create(:product, name: "5th BAG", price: "20.00", taxons: [bag]) }
  let!(:mag) { create(:taxon, name: "Mags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:mag_product) { create(:product, name: "RAILS MAG", price: "16.99", taxons: [mag]) }

  context "商品詳細について検証する" do
    before do
      visit potepan_product_path(product.id)
    end

    it "商品詳細ページの要素が正しく表示されること" do
      expect(page).to have_title("#{product.name} - BIGBAG Store")
      expect(page).to have_css(".media-body h2", text: product.name)
      expect(page).to have_css(".media-body h3", text: product.display_price)
      expect(page).to have_css(".media-body p", text: product.description)
    end

    it "商品詳細ページからHOMEページへ正しく遷移し、タイトルが正しく表示されること" do
      click_on 'BIGBAG'
      expect(current_path).to eq potepan_path
      expect(page).to have_title "BIGBAG Store"
    end

    it "カテゴリー→商品→カテゴリーへとページ遷移したときにその商品が属するカテゴリーページが表示されること" do
      visit potepan_category_path(bag.id)
      click_on bag_product.name
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(bag.id)
    end

    it "URL直打ちなどで商品ページを開いた後,カテゴリーページへ遷移したときにCATEGORIESページが表示されること" do
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxonomy.id)
      expect(page).to have_title("#{taxonomy.name} - BIGBAG Store")
    end
  end

  context "関連商品について検証する" do
    before do
      visit potepan_product_path(bag_product.id)
    end

    it "関連商品が正しく４つ表示されること" do
      within ".productsContent" do
        expect(page).to have_selector ".productBox", count: 4
        expect(page).to have_content related_products.first.name
        expect(page).to have_content related_products.first.display_price
        expect(page).to have_content related_products.fourth.name
        expect(page).to have_content related_products.fourth.display_price
      end
    end

    it "条件を満たさないものは関連商品に表示されないこと" do
      # メインの商品は関連商品に表示されないこと
      within ".productsContent" do
        expect(page).not_to have_content bag_product.name
        expect(page).not_to have_content bag_product.display_price
      end
      # ５つ目の関連商品は表示されないこと
      expect(page).not_to have_content bag_product5.name
      expect(page).not_to have_content bag_product5.display_price
      # 関連しない商品は表示されないこと
      expect(page).not_to have_content mag_product.name
      expect(page).not_to have_content mag_product.display_price
    end

    it "関連商品⇒商品詳細ページ⇒CATEGORIESページへと正しく遷移すること" do
      click_on related_products.first.name
      expect(current_path).to eq potepan_product_path(related_products.first.id)
      expect(page).to have_title("#{related_products.first.name} - BIGBAG Store")
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxonomy.id)
      expect(page).to have_title("#{taxonomy.name} - BIGBAG Store")
    end
  end
end
