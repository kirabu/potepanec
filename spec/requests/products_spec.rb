require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:product) { create(:product) }

  describe "GET #show" do
    before do
      get potepan_product_path(product.id)
    end

    it "レスポンスが成功すること" do
      expect(response).to be_successful
    end
  end
end
