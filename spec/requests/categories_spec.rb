require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }

  describe "GET #show" do
    before do
      get potepan_category_path(taxon.id)
    end

    it "レスポンスが成功すること" do
      expect(response).to be_success
    end
  end
end
