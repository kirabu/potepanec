require 'rails_helper'

RSpec.describe "ApplicationHelper", type: :helper do
  describe "full_titleメソッドテスト" do
    include ApplicationHelper
    subject { full_title(page_title) }

    context "タイトルに空文字が渡されたとき" do
      let(:page_title) { "" }

      it "「BIGBAG Store」と表示" do
        is_expected.to eq "BIGBAG Store"
      end
    end

    context "タイトルに空文字にnilが渡されたとき" do
      let(:page_title) { nil }

      it "「BIGBAG Store」と表示" do
        is_expected.to eq "BIGBAG Store"
      end
    end

    context "タイトルにアイテム名が渡されたとき" do
      let(:page_title) { "Ruby on Bag" }

      it "「Ruby on Bag - BIGBAG Store」と表示" do
        is_expected.to eq "Ruby on Bag - BIGBAG Store"
      end
    end
  end
end
